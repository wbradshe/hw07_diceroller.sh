#!/bin/bash
#
#Hw07: Do one of the random games in the book
#I am going to do the really hard #87 Let’s Roll Some Dice
#
#ALGORITHM:
#
#welcome the person to the dice rolling game
#roll the dice
#display the dice
#
##########################################################
#Pseudo Code:
#
#Echo "Lets Roll!!!!!!"
#
#echo "Die #1: RANDOMNUMBER 1-6"
#echo "Die #2: RANDOMNUMBER 1-6"
##########################################################
echo "Lets Roll!!!!!"

die1=$(( $RANDOM % 6 + 1 ))
die2=$(( $RANDOM % 6 + 1 ))
echo "Die #1: $die1"
echo "Die #2: $die2"

